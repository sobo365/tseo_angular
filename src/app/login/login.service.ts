import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  connectionsUrl: string = 'http://localhost:8080/authenticate';

  constructor(private http: HttpClient) { }

  public login(params) {
      return this.http.post(this.connectionsUrl,params,{responseType: 'text'});
  }
}
