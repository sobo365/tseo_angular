import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import {FormsModule, ReactiveFormsModule, FormControl} from '@angular/forms';
import { LoginDTO } from '../dto/LoginDTO' 
import * as jwt_decode from 'jwt-decode';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  username = new FormControl('');
  password = new FormControl('');

  invalidLogin = false;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
  }

  login() {
    const dto = new LoginDTO();
    dto.username = this.username.value;
    dto.password = this.password.value;
    this.loginService.login(dto).subscribe(
      data => {
        localStorage.setItem('jwt', data)
        var decoded = jwt_decode(data); 
        localStorage.setItem('pid',decoded.pid)
        if (decoded.scopes === 'ROLE_ADMIN') {
          this.router.navigate(['/administrator']);
        } else if (decoded.scopes === 'ROLE_STUDENT') {
          this.router.navigate(['/student']);
        } else if (decoded.scopes === 'ROLE_TEACHER') {
          this.router.navigate(['/teacher']);
        }
      }, error => {
        this.invalidLogin = true;
      }
    )
  }

}
