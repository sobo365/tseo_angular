import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { CreateCourseComponent } from '../sharedComponents/courses/createCourseComponent/create-course/create-course.component';
import { CoursesComponent } from '../sharedComponents/courses/courses.component';
import { LecturesComponent } from '../sharedComponents/lectures/lectures.component';
import { EnrollmentsComponent } from '../sharedComponents/enrollments/enrollments.component';
import { ExamsComponent } from '../sharedComponents/exams/exams.component';
import { TeachersComponent } from '../sharedComponents/teachers/teachers.component';
import { StudentsComponent } from '../sharedComponents/students/students.component';
import { AddTeacherComponent } from '../sharedComponents/teachers/add-teacher/add-teacher.component'
import { AddStudentComponent } from '../sharedComponents/students/add-student/add-student.component';
import { CreateLectureComponent } from '../sharedComponents/lectures/create-lecture/create-lecture.component';
import { TeacherLecturesDialogComponent } from '../sharedComponents/dialogs/teacherLecturesDialog/teacher-lectures-dialog/teacher-lectures-dialog.component';


const routes: Routes = [
  {path: '', component: MainComponent, children: [
    {path: 'create-course', component: CreateCourseComponent},
    {path: 'courses', component: CoursesComponent},
    {path: 'lectures', component: LecturesComponent},
    {path: 'enrollments', component: EnrollmentsComponent},
    {path: 'exams', component: ExamsComponent},
    { path: 'teachers', component: TeachersComponent},
    { path: 'students', component: StudentsComponent},
    { path: 'add-teacher', component: AddTeacherComponent },
    { path: 'add-student', component: AddStudentComponent },
    {path: 'create-lecture', component: CreateLectureComponent},
    {path: 'dialog', component: TeacherLecturesDialogComponent},
  ]},
  

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministratorRoutingModule { }
