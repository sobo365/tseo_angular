import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministratorRoutingModule } from './administrator-routing.module';
import { MainComponent } from './main/main.component';
import { ANavbarComponent } from './a-navbar/a-navbar.component';
import { ASidebarComponent } from './a-sidebar/a-sidebar.component';


@NgModule({
  declarations: [MainComponent, ANavbarComponent, ASidebarComponent],
  imports: [
    CommonModule,
    AdministratorRoutingModule
  ]
})
export class AdministratorModule { }
