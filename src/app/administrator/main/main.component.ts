import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('jwt') == null){
      this.router.navigate([''])
    } 

    let decoded = jwt_decode(localStorage.getItem('jwt'))
    if(decoded.scopes !== 'ROLE_ADMIN') {
      this.router.navigate([''])
    }
  }

}
