import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoursesComponent } from './sharedComponents/courses/courses.component';
import { ExamsComponent } from './sharedComponents/exams/exams.component';
import { EnrollmentsComponent } from './sharedComponents/enrollments/enrollments.component';
import { LecturesComponent } from './sharedComponents/lectures/lectures.component';
import { LecturesService } from './sharedComponents/lectures/lectures.service';
import { EnrollmentsService } from './sharedComponents/enrollments/enrollments.service';
import { ExamsService } from './sharedComponents/exams/exams.service';
import { CreateCourseComponent } from './sharedComponents/courses/createCourseComponent/create-course/create-course.component';
import { CreateEnrollmentComponent } from './sharedComponents/enrollments/createEnrollment/create-enrollment/create-enrollment.component';
import { CreateExamComponent } from './sharedComponents/exams/createExamComponent/create-exam/create-exam.component';
import { ExamsStudentComponent } from './sharedComponents/exams/student/exams-student/exams-student.component';
import { EnrollmentsStudentComponent } from './sharedComponents/enrollments/student/enrollments-student/enrollments-student.component';
import { StudentService } from './student/student.service';
import { ProfileComponent } from './student/profile/profile/profile.component';
import { EditProfileComponent } from './teacher/edit-profile/edit-profile.component';
import { TeacherLecturesComponent } from './sharedComponents/lectures/teacher-lectures/teacher-lectures.component';
import { TeacherProfileService } from './teacher/edit-profile/profile.service';
import { CourseService } from './sharedComponents/courses/course.service';
import { CreateLectureComponent } from './sharedComponents/lectures/create-lecture/create-lecture.component';
import { TeacherEnrollmentsComponent } from './sharedComponents/enrollments/teacher/teacher-enrollments/teacher-enrollments.component';
import { TeachersComponent } from './sharedComponents/teachers/teachers.component' 
import { StudentsComponent } from './sharedComponents/students/students.component' 
import { AddTeacherComponent} from './sharedComponents/teachers/add-teacher/add-teacher.component'
import { AddStudentComponent} from './sharedComponents/students/add-student/add-student.component'
import { ExamsStudentRegisterComponent } from './sharedComponents/exams/student/exams-student/registered/exams-student-register/exams-student-register.component';
import { ExamsTeacherComponent } from './sharedComponents/exams/exams-teacher/exams-teacher/exams-teacher.component';
import { ExamsTeacherRegisteredComponent } from './sharedComponents/exams/exams-teacher/exams-teacher/registered/exams-teacher-registered/exams-teacher-registered.component';
import { EditExamComponent } from './sharedComponents/exams/editExamComponent/edit-exam/edit-exam.component';
import { AddLectureComponent } from './sharedComponents/lectures/teacher-lectures/add-lecture/add-lecture.component';
import { TeacherLecturesDialogComponent } from './sharedComponents/dialogs/teacherLecturesDialog/teacher-lectures-dialog/teacher-lectures-dialog.component';
import { StudentDocumentsDialogComponent } from './sharedComponents/dialogs/studentDocumentsDialog/student-documents-dialog/student-documents-dialog.component'
@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    ExamsComponent,
    EnrollmentsComponent,
    LecturesComponent,
    CreateCourseComponent,
    CreateEnrollmentComponent,
    CreateExamComponent,
    CreateLectureComponent,
    ExamsStudentComponent,
    EnrollmentsStudentComponent,
    ProfileComponent,
    EditProfileComponent,
    TeacherLecturesComponent,
    TeacherEnrollmentsComponent,
    TeachersComponent,
    StudentsComponent,
    AddTeacherComponent,
    AddStudentComponent,
    AddLectureComponent,
    ExamsStudentRegisterComponent,
    ExamsTeacherComponent,
    ExamsTeacherRegisteredComponent,
    EditExamComponent,
    TeacherLecturesDialogComponent,
    StudentDocumentsDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [LecturesService, EnrollmentsService, ExamsService, StudentService, TeacherProfileService, CourseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
