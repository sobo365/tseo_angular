import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpHeaders, HttpClient, HttpResponse} from '@angular/common/http';
import { Teacher } from '../model/teacher';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class TeacherProfileService {

  teacherConnURL = 'http://localhost:8080/teachers/myTeacherAccount';
  userConnURL = 'http://localhost:8080/teachers/myUserAccount';
  teachers = 'http://localhost:8080/teachers';
  users = 'http://localhost:8080/teachers/editUser'

  headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('jwt'));  

  constructor(private http: HttpClient) { }

  getTeacherAccount(): Observable<HttpResponse<any>> {
    return this.http.get<any>(this.teacherConnURL,  { observe: 'response', headers: this.headers });
  }

  getUserAccount(): Observable<HttpResponse<any>> {
    return this.http.get<any>(this.userConnURL,  { observe: 'response', headers: this.headers });
  }

  editTeacherProfile(teacher: Teacher): Observable<HttpResponse<any>> {
    return this.http.put<any>(this.teachers, teacher,  { observe: 'response', headers: this.headers });
  }

  editUserProfile(user: User): Observable<HttpResponse<any>> {
    console.log(user)
    return this.http.put<any>(this.users, user,  { observe: 'response', headers: this.headers });
  }
}

