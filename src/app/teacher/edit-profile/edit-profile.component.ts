import { Component, OnInit, Input } from '@angular/core';
import { TeacherProfileService } from './profile.service';
import { Teacher } from '../model/teacher';
import { User } from '../model/user';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  
  teacherAccount: Teacher;
  userAccount: User;
  enableTextBox =  true;

  constructor(private teacherService: TeacherProfileService) { 
    this.teacherAccount = {} as Teacher;
    this.userAccount = {} as User;
  }

  getTeacherAccountFromService(): void {
    this.teacherService.getTeacherAccount().subscribe(
      (data: any) => {
        this.teacherAccount = data.body as Teacher;
        console.log(this.teacherAccount);
      }, error => {
        alert('There was an error. Check log for more details!')
        console.log(error)
      }
    )}

  getUserAccountFromService(): void {
    this.teacherService.getUserAccount().subscribe(
      (data: any) => {
        this.userAccount = data.body as User;
        console.log(this.userAccount);
      }, error => {
        alert('There was an error. Check log for more details!')
        console.log(error)
      }
    )}

  toggleEnable() {
      this.enableTextBox = !this.enableTextBox;
  }

  save(): void {
    this.teacherService.editTeacherProfile(this.teacherAccount).subscribe(data => console.log(data));
    this.teacherService.editUserProfile(this.userAccount).subscribe(
      data => { 
        console.log(data)
        this.userAccount.password = '';
      }
    );
    alert('Successfully modified profile!');
    console.log("promenjen nastavnik", this.teacherAccount);
    console.log("promenjen nastavnik", this.userAccount);
  }

  ngOnInit(): void {
    this.getTeacherAccountFromService();
    this.getUserAccountFromService();
  }
}