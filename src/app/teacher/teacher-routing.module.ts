import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { TeacherLecturesComponent } from '../sharedComponents/lectures/teacher-lectures/teacher-lectures.component';
import { TeacherEnrollmentsComponent } from '../sharedComponents/enrollments/teacher/teacher-enrollments/teacher-enrollments.component';
import { CreateEnrollmentComponent } from '../sharedComponents/enrollments/createEnrollment/create-enrollment/create-enrollment.component';
import { ExamsTeacherComponent } from '../sharedComponents/exams/exams-teacher/exams-teacher/exams-teacher.component';
import { ExamsTeacherRegisteredComponent } from '../sharedComponents/exams/exams-teacher/exams-teacher/registered/exams-teacher-registered/exams-teacher-registered.component';
import { EditExamComponent } from '../sharedComponents/exams/editExamComponent/edit-exam/edit-exam.component';
import { AddLectureComponent } from '../sharedComponents/lectures/teacher-lectures/add-lecture/add-lecture.component';


const routes: Routes = [
  {path: '', component: MainComponent, children: [
    {path: 'editProfile', component: EditProfileComponent},
    {path: 'teacherLectures', component: TeacherLecturesComponent},
    {path: 'teacherEnrollments', component: TeacherEnrollmentsComponent},
    {path: 'createEnrollment', component: CreateEnrollmentComponent},
    {path: 'teacherExams', component: ExamsTeacherComponent},
    {path: 'teacherRegisteredExams', component: ExamsTeacherRegisteredComponent},
    {path: 'teacherEditExam/:id', component: EditExamComponent},
    {path: 'add-lecture', component: AddLectureComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherRoutingModule { }
