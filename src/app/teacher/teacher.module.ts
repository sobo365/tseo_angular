import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeacherRoutingModule } from './teacher-routing.module';
import { MainComponent } from './main/main.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TSidebarComponent } from './t-sidebar/t-sidebar.component';
import { TNavbarComponent } from './t-navbar/t-navbar.component';

@NgModule({
  declarations: [MainComponent, TSidebarComponent, TNavbarComponent],
  imports: [
    CommonModule,
    TeacherRoutingModule,
    NgbModule
  ]
})
export class TeacherModule { }
