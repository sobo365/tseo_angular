import { Component, OnInit } from '@angular/core';
import { StudentsService } from './students.service';
import { StudentDTO } from 'src/app/student/DTO/StudentDTO';
import { Router } from '@angular/router';
import { FormControl} from '@angular/forms';
import { StudentDocumentsDialogComponent } from '../dialogs/studentDocumentsDialog/student-documents-dialog/student-documents-dialog.component';


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  students: StudentDTO[];

  displayData = []

  filter = new FormControl('');


  constructor(
    private studentsService: StudentsService, 
    private router: Router,
    private documentsDialog: StudentDocumentsDialogComponent,
    ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void { 
      this.studentsService.getAll().subscribe(
        (data: any) => {
          console.log(data.body)
          this.students = data.body;
          this.displayData = data.body;   
        }, error => {
          alert('There was an error. Check log for more details!')
          console.log(error)
        }
      )
    
    
  }

  
  search(){
    let filtered= []
    for(let i = 0; i < this.students.length; i++) {
      if(this.students[i].firstName.startsWith(this.filter.value)) {
        filtered.push(this.students[i])
      } else if (this.students[i].lastName.startsWith(this.filter.value)) {
        filtered.push(this.students[i])
      } else if (this.students[i].cardNumber.startsWith(this.filter.value)) {
        filtered.push(this.students[i])
      }
    }
    this.displayData = filtered
  }

  addStudent() {
    this.router.navigate(['administrator/add-student'])
  }

  editStudent(student) {
    this.studentsService.studentForEdit = student;
    this.router.navigate(['administrator/add-student'])
  }
  

  deleteStudent(id) {
    this.studentsService.delete(id).subscribe(
      data => {
        this.loadData()
      }, error => {
        this.loadData()
      }
    )
  }

}
