export class CourseDTO {
    public id: String;
    public firstName: String;
    public lastName: String;
    public cardNumber: String;
    public documents: any;
}
