import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { FormControl} from '@angular/forms';
import {StudentsService} from '../students.service'
import { StudentDTO } from 'src/app/student/DTO/StudentDTO';
import { error } from 'protractor';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {

  firstname = new FormControl('');
  lastname = new FormControl('');
  cardNumber = new FormControl('');

  constructor(private location: Location, private studeentsService: StudentsService) { }

  ngOnInit(): void {
    if (this.studeentsService.studentForEdit != null) {
      this.firstname = new FormControl(this.studeentsService.studentForEdit.firstName);
      this.lastname = new FormControl(this.studeentsService.studentForEdit.lastName);
      this.cardNumber = new FormControl(this.studeentsService.studentForEdit.cardNumber);
    }
  }

  ngOnDestroy() {
    this.studeentsService.studentForEdit = null;
  }

  submit() {
    if (this.studeentsService.studentForEdit == null) {
      let dto = new StudentDTO;
      dto.firstName = this.firstname.value;
      dto.lastName = this.lastname.value;
      dto.cardNumber = this.cardNumber.value;
      this.studeentsService.insert(dto).subscribe(
        data => {
          this.location.back();
        }, error => {
          alert('error')
          console.error(error)
        }
      )
    } else {
      let dto = new StudentDTO;
      dto.id = this.studeentsService.studentForEdit.id;
      dto.firstName = this.firstname.value;
      dto.lastName = this.lastname.value;
      dto.cardNumber = this.cardNumber.value;
      this.studeentsService.update(dto).subscribe(
        data => {
          this.location.back();
        }, error => {
          alert('error');
          console.log(error)
        }
      )
    }
    
  }

  backBtn(){
    this.location.back();
  }

}
