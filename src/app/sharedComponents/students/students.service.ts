import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { StudentDTO } from 'src/app/student/DTO/StudentDTO';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  connectionURL = 'http://localhost:8080/students';

  studentForEdit:StudentDTO;

  headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + localStorage.getItem('jwt'))

  constructor(private http: HttpClient) { }

  getAll(): Observable<HttpResponse<StudentDTO>> {
    return this.http.get<StudentDTO>(this.connectionURL,  { observe: 'response', headers: this.headers });
  }

  insert(params){
    return this.http.post(this.connectionURL,params, { observe: 'response', headers: this.headers });
  } 

  update(params){
    return this.http.put(this.connectionURL,params, { observe: 'response', headers: this.headers });
  } 

  delete(id: String) {
    return this.http.delete(`${this.connectionURL}/${id}`, { observe: 'response', headers: this.headers});
  }
}
