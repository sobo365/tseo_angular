import { StudentDTO } from "../../../student/DTO/StudentDTO";
import { CourseDTO } from "../../courses/DTO/CourseDTO";
import { Teacher } from 'src/app/teacher/model/teacher';

export class EnrollmentDTO {
    public id: Number;
    public date: Date;
    public courseDTO: CourseDTO;
    public studentDTO: StudentDTO;
    public teacherDTO: Teacher;
}