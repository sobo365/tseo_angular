export class CreateEnrollmentDTO {
    public date: Date;
    public courseId: number;
    public studentId: String;
    public teacherId: String;
}