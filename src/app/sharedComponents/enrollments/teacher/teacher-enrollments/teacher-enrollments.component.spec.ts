import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherEnrollmentsComponent } from './teacher-enrollments.component';

describe('TeacherEnrollmentsComponent', () => {
  let component: TeacherEnrollmentsComponent;
  let fixture: ComponentFixture<TeacherEnrollmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherEnrollmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherEnrollmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
