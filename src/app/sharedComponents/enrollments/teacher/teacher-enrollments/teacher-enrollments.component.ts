import { Component, OnInit } from '@angular/core';
import { EnrollmentDTO } from '../../DTO/EnrollmentDTO';
import { EnrollmentsService } from '../../enrollments.service';
import { LecturesService } from 'src/app/sharedComponents/lectures/lectures.service';
import { Lecture } from 'src/app/sharedComponents/model/lecture';
import { Router } from '@angular/router';

@Component({
  selector: 'app-teacher-enrollments',
  templateUrl: './teacher-enrollments.component.html',
  styleUrls: ['./teacher-enrollments.component.css']
})
export class TeacherEnrollmentsComponent implements OnInit {

  enrollments: EnrollmentDTO[];
  lectures: Lecture[];

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  constructor(private enrollmentsService: EnrollmentsService, private lectureService: LecturesService,
              private router: Router) { }

  getEnrollments(): void {
    this.enrollmentsService.getEnrollments().subscribe(
      (data: any) => {
        this.enrollments = data.body;
      }
    )}

  getLecturesFromService(): void {

    this.lectureService.getLectures().subscribe(
      (updatedLectures) => {
        this.lectures = updatedLectures.body;
        console.log(`this.lectures=${JSON.stringify(this.lectures)}`);
      }
    );
  };

  ngOnInit(): void {
    this.getEnrollments();
    this.getLecturesFromService();
  }

  teacherCreateEnrollment() {
    this.router.navigate(['teacher/createEnrollment'])
  }
}