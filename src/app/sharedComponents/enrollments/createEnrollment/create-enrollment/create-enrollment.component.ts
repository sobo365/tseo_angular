import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { EnrollmentsService } from '../../enrollments.service';
import { CreateEnrollmentDTO } from '../../DTO/CreateEnrollmentDTO';
import { StudentService } from 'src/app/student/student.service';
import { StudentDTO } from 'src/app/student/DTO/StudentDTO';
import { LecturesService } from 'src/app/sharedComponents/lectures/lectures.service';
import { Lecture } from 'src/app/sharedComponents/model/lecture';
import { Teacher } from 'src/app/teacher/model/teacher';

@Component({
  selector: 'app-create-enrollment',
  templateUrl: './create-enrollment.component.html',
  styleUrls: ['./create-enrollment.component.css']
})
export class CreateEnrollmentComponent implements OnInit {
  students: StudentDTO[];
  lectures: Lecture[];
  teachers: Teacher[];
  date = new FormControl('');
  courseId = new FormControl;
  studentId = new FormControl('');
  teacherId = new FormControl('');

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  constructor(private enrollmentService: EnrollmentsService, private studentService: StudentService,
              private lectureService: LecturesService) { }

  ngOnInit(): void {
    this.getLecturesFromService();
    this.getStudents();
    this.getTeachersFromService()
  }

  onSubmitClick() {
    const params = new CreateEnrollmentDTO();
    params.date = this.date.value;
    params.courseId = this.courseId.value;
    params.studentId = this.studentId.value;
    params.teacherId = this.teacherId.value;

    this.enrollmentService.insert(params).subscribe(
      data => {
        alert('ok')
      }, error => {
        alert('There was an error.')
        console.log(error)
      }
    )
  }

  getStudents(): void {
    this.studentService.getStudents().subscribe(
      (data: any) => {
        this.students = data.body;
      }
  )}

  getLecturesFromService(): void {

    this.lectureService.getLectures().subscribe(
      (updatedLectures) => {
        this.lectures = updatedLectures.body;
        console.log(`this.lectures=${JSON.stringify(this.lectures)}`);
      }
    );
  };

  getTeachersFromService(): void {
    this.lectureService.getTeachers().subscribe(
      (updatedTeachers) => {
        this.teachers = updatedTeachers.body;
        console.log(`this.teachers=${JSON.stringify(this.teachers)}`);
      }
    );
  };
}