import { Component, OnInit } from '@angular/core';
import { EnrollmentsService } from './enrollments.service';
import { EnrollmentDTO } from '../enrollments/DTO/EnrollmentDTO';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enrollments',
  templateUrl: './enrollments.component.html',
  styleUrls: ['./enrollments.component.css']
})
export class EnrollmentsComponent implements OnInit {

  enrollments: EnrollmentDTO[];

  constructor(private enrollmentsService: EnrollmentsService, private router: Router) { }

  getEnrollments(): void {
    this.enrollmentsService.getEnrollments().subscribe(
      (data: any) => {
        this.enrollments = data.body;
      }
  )}
  
  deleteEnrollment(id: number): void {
    this.enrollmentsService.deleteEnrollment(id).subscribe(
        () => this.getEnrollments() 
    );
  }

  ngOnInit(): void {
    this.getEnrollments();
  }
}
