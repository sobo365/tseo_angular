import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { EnrollmentDTO } from './DTO/EnrollmentDTO';
import { CreateEnrollmentDTO } from './DTO/CreateEnrollmentDTO';

@Injectable({
  providedIn: 'root'
})
export class EnrollmentsService {
  enrollmentsURL = 'http://localhost:8080/enrollments';

  headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('jwt'))
  constructor(private http: HttpClient) { }

  getEnrollments(): Observable<HttpResponse<Array<EnrollmentDTO>>> {
    return this.http.get<Array<EnrollmentDTO>>(this.enrollmentsURL,  { observe: 'response', headers: this.headers });
  }

  insert(enrollmentDTO: CreateEnrollmentDTO): Observable<HttpResponse<CreateEnrollmentDTO>> {
    return this.http.post<CreateEnrollmentDTO>(this.enrollmentsURL, enrollmentDTO, { observe: 'response', headers: this.headers });
  }
  
  deleteEnrollment(id: number): Observable<HttpResponse<String>> {
    return this.http.delete<String>(`${this.enrollmentsURL}/${id}`, { observe: 'response', headers: this.headers});
  }
}
