import { Component, OnInit } from '@angular/core';
import { EnrollmentDTO } from '../../DTO/EnrollmentDTO';
import { EnrollmentsService } from '../../enrollments.service';

@Component({
  selector: 'app-enrollments-student',
  templateUrl: './enrollments-student.component.html',
  styleUrls: ['./enrollments-student.component.css']
})
export class EnrollmentsStudentComponent implements OnInit {

  enrollments: EnrollmentDTO[];

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  constructor(private enrollmentsService: EnrollmentsService) { }

  getEnrollments(): void {
    this.enrollmentsService.getEnrollments().subscribe(
      (data: any) => {
        this.enrollments = data.body;
      }
    )}

  ngOnInit(): void {
    this.getEnrollments();
  }
}