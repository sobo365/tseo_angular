import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollmentsStudentComponent } from './enrollments-student.component';

describe('EnrollmentsStudentComponent', () => {
  let component: EnrollmentsStudentComponent;
  let fixture: ComponentFixture<EnrollmentsStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollmentsStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollmentsStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
