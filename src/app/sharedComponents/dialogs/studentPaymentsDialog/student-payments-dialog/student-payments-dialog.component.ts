import { Component, OnInit, Injectable } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-student-payments-dialog',
  templateUrl: './student-payments-dialog.component.html',
  styleUrls: ['./student-payments-dialog.component.css']
})

@Injectable({
  providedIn: 'root'
})
export class StudentPaymentsDialogComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
  }

  ngOnInit(): void {
  }

}
