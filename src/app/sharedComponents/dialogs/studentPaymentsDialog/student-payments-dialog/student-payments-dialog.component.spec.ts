import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentPaymentsDialogComponent } from './student-payments-dialog.component';

describe('StudentPaymentsDialogComponent', () => {
  let component: StudentPaymentsDialogComponent;
  let fixture: ComponentFixture<StudentPaymentsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentPaymentsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentPaymentsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
