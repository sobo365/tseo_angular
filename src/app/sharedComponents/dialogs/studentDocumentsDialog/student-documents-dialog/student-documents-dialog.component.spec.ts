import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentDocumentsDialogComponent } from './student-documents-dialog.component';

describe('StudentDocumentsDialogComponent', () => {
  let component: StudentDocumentsDialogComponent;
  let fixture: ComponentFixture<StudentDocumentsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentDocumentsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentDocumentsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
