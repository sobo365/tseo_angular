import { Component, OnInit, Injectable, Input } from '@angular/core';
import {NgbModal, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { DocumentsService } from '../documents.service';
import { DomSanitizer } from '@angular/platform-browser';
import { resolve } from 'dns';
import { read } from 'fs';

@Component({
  selector: 'app-student-documents-dialog',
  templateUrl: './student-documents-dialog.component.html',
  styleUrls: ['./student-documents-dialog.component.css']
})

@Injectable({
  providedIn: 'root'
})
export class StudentDocumentsDialogComponent implements OnInit {

  constructor(private service: DocumentsService, private sanitizer: DomSanitizer) {}

  showModal = false;

  documents = [];

  pdf: string ;


  imageSource;

  @Input() studentId: String;

  openModal() {
    this.loadDocuments();
    this.showModal = true;
  }

  closeModal() {
    this.showModal = false;
  }

  ngOnInit(): void {
  }

  toggleIframe = (position) => {
    this.documents[position].display = !this.documents[position].display;
  }

  loadDocuments = () =>  {
    this.documents = [];
    this.service.getDocuments(this.studentId).subscribe(
      (data: any) => {
        for (let i = 0; i < data.body.length; i++ ) {
          console.log(this.documents)
          let d = {
            position: i,
            name: data.body[i].name,
            data: this.sanitizer.bypassSecurityTrustResourceUrl(`data:application/pdf;base64, ${data.body[i].path}`),
            display: false,
          }
          this.documents.push(d)
        }
      }, error => {
        console.log(error)
      }
    )
  }

}
