import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpHeaders, HttpClient, HttpResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {

  connectionURL = 'http://localhost:8080/documents';

  headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('jwt'));  

  constructor(private http: HttpClient) { }

  getDocuments(studentId: String) {
    return this.http.get(`${this.connectionURL}/${studentId}`, { observe: 'response', headers: this.headers});
  }

  insert(params) {
    return this.http.post(this.connectionURL, params, { observe: 'response', headers: this.headers })
  }
}
