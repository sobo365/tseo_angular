import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherLecturesDialogComponent } from './teacher-lectures-dialog.component';

describe('TeacherLecturesDialogComponent', () => {
  let component: TeacherLecturesDialogComponent;
  let fixture: ComponentFixture<TeacherLecturesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherLecturesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherLecturesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
