import { Component, OnInit, Inject, Injectable, Input } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { TeachersService } from '../../../teachers/teachers.service'

@Component({
  selector: 'app-teacher-lectures-dialog',
  templateUrl: './teacher-lectures-dialog.component.html',
  styleUrls: ['./teacher-lectures-dialog.component.css']
})

@Injectable({
  providedIn: 'root'
})
export class TeacherLecturesDialogComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private teacherService: TeachersService) {}

  content : any;

  showModal =  false;

  lectures = [];

  courses = []

  @Input() teacherId: String;

  @Input() firstName: String;

  @Input() lastName: String;

  openModal() {
    this.loadLectures();
    this.showModal = true;
  }

  closeModal() {
    this.showModal = false;
  }

  loadLectures() {
    this.lectures = []
    this.teacherService.getTeacherLectures(this.teacherId).subscribe(
      (data : any) => {
        console.log(data)
        for(let i = 0; i < data.body.length; i++ ) {
          this.lectures.push(data.body[i]);
        }
      }, error => {
        console.log(error)
      }
    )
  }
  
  ngOnInit(): void {
    
  }

}
 