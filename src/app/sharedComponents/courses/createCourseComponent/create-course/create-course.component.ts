import { Component, OnInit, OnDestroy } from '@angular/core';
import { CourseService } from '../../course.service';
import { FormControl} from '@angular/forms';
import { CreateCourseDTO } from '../../dto/CreateCourseDTO';
import { error } from 'protractor';
import {Location} from '@angular/common';
import { CourseDTO } from '../../DTO/CourseDTO';

@Component({
  selector: 'app-create-course',
  templateUrl: './create-course.component.html',
  styleUrls: ['./create-course.component.css']
})
export class CreateCourseComponent implements OnInit {

  courseName = new FormControl('');

  constructor(private location: Location, private courseService: CourseService) { }

  ngOnInit(): void {
    if(this.courseService.CourseForEdit != null) {
      this.courseName =  new FormControl(this.courseService.CourseForEdit.name)
    }
  }

  ngOnDestroy() {
    this.courseService.CourseForEdit = null;
  }

  onSubmitClick() {
    if(this.courseService.CourseForEdit == null) {
      const params = new CreateCourseDTO();
      params.name = this.courseName.value;
      this.courseService.insert(params).subscribe(
        data => {
          this.location.back();
        }, error => {
          alert('There was an error. Check log for more details!')
          console.log(error)
        }
      )
    } else {
      const dto = new CourseDTO();
      dto.id = this.courseService.CourseForEdit.id;
      dto.name = this.courseName.value;
      this.courseService.update(dto).subscribe(
        data => {
          this.location.back();
        }, error => {
          alert('not ok')
          console.log(error)
        }
      )
    }
  }

  backBtn(){
    this.location.back();
  }

}
