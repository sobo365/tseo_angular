import { Component, OnInit } from '@angular/core';
import { CourseDTO } from './dto/CourseDTO';
import { CourseService } from './course.service';
import { error } from 'protractor';
import { Router } from '@angular/router';
import { FormControl} from '@angular/forms';
import { identifierModuleUrl } from '@angular/compiler';


@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  courses : CourseDTO[];
  displayData = []

  filter = new FormControl('');

  sortState = true;

  constructor(private courseService: CourseService, private router: Router) { }

  ngOnInit(): void {
    this.loadData();
    this.courseService.CourseForEdit = null;
  }

  loadData(): void {
    this.courseService.getAll().subscribe(
      (data: any) => {
        this.courses = data.body;
        this.displayData = data.body;
      }, error => {
        alert('There was an error. Check log for more details!')
        console.log(error)
      }
    )}

    search(){
      let filtered= []
      for(let i = 0; i < this.courses.length; i++) {
        if(this.courses[i].name.startsWith(this.filter.value)) {
          filtered.push(this.courses[i])
        }
      }
      this.displayData = filtered
    }

    sort() {
        this.sortState = !this.sortState;
        if(this.sortState){
          this.displayData.sort(compareA)
        } else {
          this.displayData.sort(compareB)
        }
        
        function compareB(a, b) {
          if (a.name > b.name) return 1;
          if (b.name > a.name) return -1;
        
          return 0;
        }

        function compareA(a, b) {
          if (a.name < b.name) return 1;
          if (b.name < a.name) return -1;
        
          return 0;
        }
     
    }

    sortA() {
      this.displayData.sort(compare)

        function compare(a, b) {
          if (a.name < b.name) return 1;
          if (b.name < a.name) return -1;
        
          return 0;
        }
    }

    addCourse() {
      this.router.navigate(['administrator/create-course'])
    }

    editCourse(course) {
      this.courseService.CourseForEdit = course;
      this.router.navigate(['administrator/create-course'])
    }

    deleteCourse(id) {
      this.courseService.delete(id).subscribe(
        data => {
          this.loadData()
        }, error => {
          this.loadData()
        }
      )
    }

}
