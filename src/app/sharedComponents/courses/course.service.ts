import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { CourseDTO } from './dto/CourseDTO';
import { CreateCourseDTO } from './dto/CreateCourseDTO';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  connectionURL = 'http://localhost:8080/courses';

  CourseForEdit: CourseDTO;

  headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + localStorage.getItem('jwt'))

  constructor(private http: HttpClient) { }


  getAll(): Observable<HttpResponse<CourseDTO>> {
    return this.http.get<CourseDTO>(this.connectionURL,  { observe: 'response', headers: this.headers });
  }

  insert(courseDTO: CreateCourseDTO): Observable<HttpResponse<CreateCourseDTO>> {
    return this.http.post<CreateCourseDTO>(this.connectionURL, courseDTO, { observe: 'response', headers: this.headers })
  }

  update(courseDTO: CourseDTO): Observable<HttpResponse<CourseDTO>> {
    return this.http.put<CourseDTO>(this.connectionURL, courseDTO, { observe: 'response', headers: this.headers })
  }

  delete(id: number) {
    return this.http.delete(`${this.connectionURL}/${id}`, { observe: 'response', headers: this.headers});
  }

}
