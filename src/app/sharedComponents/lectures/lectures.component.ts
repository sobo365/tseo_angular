import {Component, OnInit} from '@angular/core';
import {Lecture} from '../model/lecture';
import {LecturesService} from './lectures.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lectures',
  templateUrl: './lectures.component.html',
  styleUrls: ['./lectures.component.css']
})
export class LecturesComponent implements OnInit {

  lectures: Lecture[];

  constructor(private lectureService: LecturesService, private router: Router) {
  }

  getLecturesFromService(): void {

    this.lectureService.getLectures().subscribe(
      (updatedLectures) => {
        this.lectures = updatedLectures.body;
        console.log(`this.lectures=${JSON.stringify(this.lectures)}`);
      }
    );
  };

  deleteLecture(id: number): void {
    this.lectureService.deleteLecture(id).subscribe(
      () => this.getLecturesFromService() 
  );
}

  addLecture() {
    this.router.navigate(['administrator/create-lecture'])
  }

  ngOnInit(): void {
    this.getLecturesFromService();
  }
}
