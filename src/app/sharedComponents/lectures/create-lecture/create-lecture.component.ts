import { Component, OnInit } from '@angular/core';
import { LecturesService } from '../lectures.service';
import { CourseService } from '../../courses/course.service';
import { CourseDTO } from '../../courses/DTO/CourseDTO';
import { Teacher } from 'src/app/teacher/model/teacher';
import { FormControl } from '@angular/forms';
import { CreateLectureDTO } from '../../model/createLecture';
import { Location } from '@angular/common';

@Component({
  selector: 'app-create-lecture',
  templateUrl: './create-lecture.component.html',
  styleUrls: ['./create-lecture.component.css']
})
export class CreateLectureComponent implements OnInit {

  teachers: Teacher[];
  courses: CourseDTO[];
  roles: any;
  teacherId = new FormControl('');
  courseId = new FormControl('');
  roleId = new FormControl('');

  constructor(private lectureService: LecturesService, private courseService: CourseService, private location: Location) { }

  getTeachersFromService(): void {
    this.lectureService.getTeachers().subscribe(
      (updatedTeachers) => {
        this.teachers = updatedTeachers.body;
        console.log(`this.teachers=${JSON.stringify(this.teachers)}`);
      }
    );
  };

  getCoursesFromService(): void {
    this.courseService.getAll().subscribe(
      (updatedCourses: any) => {
        this.courses = updatedCourses.body;
        console.log(`this.courses=${JSON.stringify(this.courses)}`);
      }
    );
  };

  getRolesFromService(): void {
    this.lectureService.getRoles().subscribe(
      (data: any) => {
        this.roles = data.body;
      }, error => {
        alert("err")
      }
    )
  }

  onSubmitClick() {
    const params = new CreateLectureDTO();
    params.teacherId = this.teacherId.value;
    params.courseId = this.courseId.value;
    params.roleId = this.roleId.value;

    this.lectureService.insert(params).subscribe(
      data => {
        this.location.back();
      }, error => {
        alert('There was an error.')
        console.log(error)
      }
    )
  }

  backBtn(){
    this.location.back();
  }

  ngOnInit(): void {
    this.getTeachersFromService();
    this.getCoursesFromService();
    this.getRolesFromService();
    }
  }
