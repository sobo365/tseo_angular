import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Lecture} from '../model/lecture';
import {HttpHeaders, HttpClient, HttpResponse} from '@angular/common/http';
import {catchError, tap, map} from 'rxjs/operators';
import { Teacher } from 'src/app/teacher/model/teacher';
import { CreateLectureDTO } from '../model/createLecture';

@Injectable({
  providedIn: 'root'
})
export class LecturesService {

  connectionURL = 'http://localhost:8080/lectures';
  teachersURL = 'http://localhost:8080/teachers'

  headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('jwt'));  

  constructor(private http: HttpClient) { }

  getLectures(): Observable<HttpResponse<Array<Lecture>>> {
    return this.http.get<Array<Lecture>>(this.connectionURL,  { observe: 'response', headers: this.headers });
  }

  getRoles(): Observable<HttpResponse<Array<any>>> {
    return this.http.get<Array<any>>(`${this.connectionURL}/roles`,  { observe: 'response', headers: this.headers });
  }

  getTeachers(): Observable<HttpResponse<Array<Teacher>>> {
    return this.http.get<Array<Teacher>>(this.teachersURL,  { observe: 'response', headers: this.headers });
  }

  insert(createLecture: CreateLectureDTO): Observable<HttpResponse<CreateLectureDTO>> {
    return this.http.post<CreateLectureDTO>(this.connectionURL, createLecture, { observe: 'response', headers: this.headers })
  }

  deleteLecture(id: number): Observable<HttpResponse<String>> {
    return this.http.delete<String>(`${this.connectionURL}/${id}`, { observe: 'response', headers: this.headers});
  }
}

