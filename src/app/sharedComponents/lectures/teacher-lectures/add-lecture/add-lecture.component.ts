import { Component, OnInit } from '@angular/core';
import { Teacher } from 'src/app/teacher/model/teacher';
import { Lecture } from 'src/app/sharedComponents/model/lecture';
import { FormControl } from '@angular/forms';
import { LecturesService } from '../../lectures.service';
import { CreateLectureDTO } from 'src/app/sharedComponents/model/createLecture';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-lecture',
  templateUrl: './add-lecture.component.html',
  styleUrls: ['./add-lecture.component.css']
})
export class AddLectureComponent implements OnInit {

  teachers: Teacher[];
  lectures: Lecture[];
  teacherId = new FormControl('');
  lectureCourseId = new FormControl('');

  constructor(private lectureService: LecturesService, private location: Location) { }

  getTeachersFromService(): void {
    this.lectureService.getTeachers().subscribe(
      (updatedTeachers) => {
        this.teachers = updatedTeachers.body;
        console.log(`this.teachers=${JSON.stringify(this.teachers)}`);
      }
    );
  };

  getLecturesFromService(): void {

    this.lectureService.getLectures().subscribe(
      (updatedLectures) => {
        this.lectures = updatedLectures.body;
        console.log(`this.lectures=${JSON.stringify(this.lectures)}`);
      }
    );
  };

  onSubmitClick() {
    const params = new CreateLectureDTO();
    params.teacherId = this.teacherId.value;
    params.courseId = this.lectureCourseId.value;

    this.lectureService.insert(params).subscribe(
      data => {
        alert('Lecture successfully added!')
      }, error => {
        alert('There was an error.')
        console.log(error)
      }
    )
  }

  backBtn() {
    this.location.back();
  }

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  ngOnInit(): void {
    this.getTeachersFromService();
    this.getLecturesFromService();
    }
  }