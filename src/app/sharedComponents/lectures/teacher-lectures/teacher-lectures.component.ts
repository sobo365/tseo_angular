import { Component, OnInit } from '@angular/core';
import { Lecture } from '../../model/lecture';
import { LecturesService } from '../lectures.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-teacher-lectures',
  templateUrl: './teacher-lectures.component.html',
  styleUrls: ['./teacher-lectures.component.css']
})
export class TeacherLecturesComponent implements OnInit {

  teacherLectures: Lecture[];

  constructor(private lectureService: LecturesService, private router: Router) {
  }

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
}

  getLecturesFromService(): void {

    this.lectureService.getLectures().subscribe(
      (updatedLectures) => {
        this.teacherLectures = updatedLectures.body;
        console.log(`this.lectures=${JSON.stringify(this.teacherLectures)}`);
      }
    );
  };

  ngOnInit(): void {
    console.log("ok");
    this.getLecturesFromService();
  }

  addLecture() {
    this.router.navigate(['teacher/add-lecture'])
  }

  deleteLecture(id: number): void {
    this.lectureService.deleteLecture(id).subscribe(
      () => this.getLecturesFromService() 
  );
  }
}