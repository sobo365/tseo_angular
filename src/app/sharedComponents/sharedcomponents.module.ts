import { CommonModule } from "@angular/common";
import { NgModule } from '@angular/core';
import { CreateLectureComponent } from './lectures/create-lecture/create-lecture.component';
import { TeacherEnrollmentsComponent } from './enrollments/teacher/teacher-enrollments/teacher-enrollments.component';
import { TeachersComponent } from '../sharedComponents/teachers/teachers.component' 
import { StudentsComponent } from '../sharedComponents/students/students.component';
import { AddTeacherComponent } from './teachers/add-teacher/add-teacher.component';
import { AddStudentComponent } from './students/add-student/add-student.component' 
import { EnrollmentsComponent } from './enrollments/enrollments.component';
import { ExamsStudentRegisterComponent } from './exams/student/exams-student/registered/exams-student-register/exams-student-register.component';
import { ExamsStudentComponent } from './exams/student/exams-student/exams-student.component';
import { ExamsTeacherComponent } from './exams/exams-teacher/exams-teacher/exams-teacher.component';
import { EditExamComponent } from './exams/editExamComponent/edit-exam/edit-exam.component';
import { ExamsTeacherRegisteredComponent } from './exams/exams-teacher/exams-teacher/registered/exams-teacher-registered/exams-teacher-registered.component';
import { AddLectureComponent } from './lectures/teacher-lectures/add-lecture/add-lecture.component';
import { TeacherLecturesDialogComponent } from './dialogs/teacherLecturesDialog/teacher-lectures-dialog/teacher-lectures-dialog.component';
import { StudentDocumentsDialogComponent } from './dialogs/studentDocumentsDialog/student-documents-dialog/student-documents-dialog.component';
import { StudentPaymentsDialogComponent } from './dialogs/studentPaymentsDialog/student-payments-dialog/student-payments-dialog.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CreateLectureComponent,
    EnrollmentsComponent,
    TeacherEnrollmentsComponent,
    TeachersComponent,
    StudentsComponent,
    AddTeacherComponent,
    AddStudentComponent,
    ExamsStudentRegisterComponent,
    ExamsStudentComponent,
    ExamsTeacherComponent,
    EditExamComponent,
    ExamsTeacherRegisteredComponent,
    AddLectureComponent,
    TeacherLecturesDialogComponent,
    StudentDocumentsDialogComponent,
    StudentPaymentsDialogComponent
  ]
})

export class SharedComponents { }