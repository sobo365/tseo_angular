import { Component, OnInit } from '@angular/core';
import { ExamsService } from './exams.service';
import { ExamDTO } from '../exams/DTO/ExamDTO';

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.css']
})
export class ExamsComponent implements OnInit {
  exams: ExamDTO[];

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  constructor(private examsService: ExamsService) { }

  getExams(): void {
    this.examsService.getExams().subscribe(
      (data: any) => {
        this.exams = data.body;
      }
    )}

  ngOnInit(): void {
    this.getExams();
  }

}
