import { Component, OnInit } from '@angular/core';
import { ExamDTO } from '../../DTO/ExamDTO';
import { ExamsService } from '../../exams.service';
import { LecturesService } from 'src/app/sharedComponents/lectures/lectures.service';
import { Lecture } from 'src/app/sharedComponents/model/lecture';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exams-teacher',
  templateUrl: './exams-teacher.component.html',
  styleUrls: ['./exams-teacher.component.css']
})
export class ExamsTeacherComponent implements OnInit {
  exams: ExamDTO[];
  lectures: Lecture[];

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  constructor(private examsService: ExamsService, private lectureService: LecturesService, private router: Router) { }

  getExams(): void {
    this.examsService.getExams().subscribe(
      (data: any) => {
        this.exams = data.body;
      }
  )}

  getLectures(): void {

    this.lectureService.getLectures().subscribe(
      (updatedLectures) => {
        this.lectures = updatedLectures.body;
        console.log(`this.lectures=${JSON.stringify(this.lectures)}`);
      }
    );
  };

  registeredExams() {
    this.router.navigate(['teacher/teacherRegisteredExams'])
  }

  ngOnInit(): void {
    this.getExams();
    this.getLectures();
  }

}