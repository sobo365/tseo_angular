import { Component, OnInit } from '@angular/core';
import { ExamDTO } from 'src/app/sharedComponents/exams/DTO/ExamDTO';
import { Lecture } from 'src/app/sharedComponents/model/lecture';
import { ExamsService } from 'src/app/sharedComponents/exams/exams.service';
import { LecturesService } from 'src/app/sharedComponents/lectures/lectures.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exams-teacher-registered',
  templateUrl: './exams-teacher-registered.component.html',
  styleUrls: ['./exams-teacher-registered.component.css']
})
export class ExamsTeacherRegisteredComponent implements OnInit {
  exams: ExamDTO[];
  lectures: Lecture[];

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  constructor(private examsService: ExamsService, private lectureService: LecturesService, private router: Router) { }

  getExams(): void {
    this.examsService.getExams().subscribe(
      (data: any) => {
        this.exams = data.body;
      }
  )}

  editExam(exam){
    this.examsService.examForEdit = exam;
    this.router.navigate(['teacher/teacherEditExam', exam.id]);
  }

  getLectures(): void {

    this.lectureService.getLectures().subscribe(
      (updatedLectures) => {
        this.lectures = updatedLectures.body;
        console.log(`this.lectures=${JSON.stringify(this.lectures)}`);
      }
    );
  };

  ngOnInit(): void {
    this.getExams();
    this.getLectures();
  }

  passedExams() {
    this.router.navigate(['teacher/teacherExams'])
  }
}