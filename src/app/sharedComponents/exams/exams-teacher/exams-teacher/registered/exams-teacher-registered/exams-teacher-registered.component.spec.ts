import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsTeacherRegisteredComponent } from './exams-teacher-registered.component';

describe('ExamsTeacherRegisteredComponent', () => {
  let component: ExamsTeacherRegisteredComponent;
  let fixture: ComponentFixture<ExamsTeacherRegisteredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsTeacherRegisteredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsTeacherRegisteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
