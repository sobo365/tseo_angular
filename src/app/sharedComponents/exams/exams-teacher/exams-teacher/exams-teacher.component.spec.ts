import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsTeacherComponent } from './exams-teacher.component';

describe('ExamsTeacherComponent', () => {
  let component: ExamsTeacherComponent;
  let fixture: ComponentFixture<ExamsTeacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsTeacherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
