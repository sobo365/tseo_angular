import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsStudentRegisterComponent } from './exams-student-register.component';

describe('ExamsStudentRegisterComponent', () => {
  let component: ExamsStudentRegisterComponent;
  let fixture: ComponentFixture<ExamsStudentRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsStudentRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsStudentRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
