import { Component, OnInit } from '@angular/core';
import { ExamDTO } from 'src/app/sharedComponents/exams/DTO/ExamDTO';
import { ExamsService } from 'src/app/sharedComponents/exams/exams.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exams-student-register',
  templateUrl: './exams-student-register.component.html',
  styleUrls: ['./exams-student-register.component.css']
})
export class ExamsStudentRegisterComponent implements OnInit {
  exams: ExamDTO[];

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  constructor(private examsService: ExamsService, private router: Router) { }

  getExams(): void {
    this.examsService.getExams().subscribe(
      (data: any) => {
        this.exams = data.body;
      }
  )}

  deleteExam(id: number): void {
    this.examsService.deleteExam(id).subscribe(
        () => this.getExams() 
    );
  }

  ngOnInit(): void {
    this.getExams();
  }

  registerExam() {
    this.router.navigate(['student/registerExam'])
  }

  passedExams() {
    this.router.navigate(['student/exams'])
  }
}