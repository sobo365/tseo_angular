import { Component, OnInit } from '@angular/core';
import { ExamDTO } from '../../DTO/ExamDTO';
import { ExamsService } from '../../exams.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exams-student',
  templateUrl: './exams-student.component.html',
  styleUrls: ['./exams-student.component.css']
})
export class ExamsStudentComponent implements OnInit {

  exams: ExamDTO[];

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  constructor(private examsService: ExamsService, private router: Router) { }

  getExams(): void {
    this.examsService.getExams().subscribe(
      (data: any) => {
        this.exams = data.body;
      }
    )}

  ngOnInit(): void {
    this.getExams();
  }

  registeredExams() {
    this.router.navigate(['student/examsStudentRegister'])
  }

  registerExam() {
    this.router.navigate(['student/registerExam'])
  }
}