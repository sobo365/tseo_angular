import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsStudentComponent } from './exams-student.component';

describe('ExamsStudentComponent', () => {
  let component: ExamsStudentComponent;
  let fixture: ComponentFixture<ExamsStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
