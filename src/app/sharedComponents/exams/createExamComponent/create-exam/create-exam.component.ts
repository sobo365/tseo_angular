import { Component, OnInit } from '@angular/core';
import { StudentDTO } from 'src/app/student/DTO/StudentDTO';
import { FormControl } from '@angular/forms';
import { StudentService } from 'src/app/student/student.service';
import { CreateExamDTO } from '../../DTO/CreateExamDTO';
import { ExamsService } from '../../exams.service';
import {Location} from '@angular/common';
import { EnrollmentsService } from 'src/app/sharedComponents/enrollments/enrollments.service';
import { EnrollmentDTO } from 'src/app/sharedComponents/enrollments/DTO/EnrollmentDTO';

@Component({
  selector: 'app-create-exam',
  templateUrl: './create-exam.component.html',
  styleUrls: ['./create-exam.component.css']
})
export class CreateExamComponent implements OnInit {
  students: StudentDTO[];
  enrollments: EnrollmentDTO[];
  courseId = new FormControl('');
  studentId = new FormControl('');

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  constructor(private examService: ExamsService, private studentService: StudentService, private enrollmentsService: EnrollmentsService, private location: Location) { }

  ngOnInit(): void {
    this.getStudents();
    this.getEnrollmentsFromService();
  }

  onSubmitClick() {
    const params = new CreateExamDTO();
    params.courseId = this.courseId.value;
    params.studentId = this.studentId.value;

    this.examService.insert(params).subscribe(
      data => {
        this.location.back();
      }, error => {
        alert('There was an error.')
        console.log(error)
      }
    )
  }

  getStudents(): void {
    this.studentService.getStudents().subscribe(
      (data: any) => {
        this.students = data.body;
      }
  )}

  getEnrollmentsFromService(): void {
    this.enrollmentsService.getEnrollments().subscribe(
      (data: any) => {
        this.enrollments = data.body;
      }
  )}
}