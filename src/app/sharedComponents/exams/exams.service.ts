import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { ExamDTO } from './DTO/ExamDTO';
import { CreateExamDTO } from './DTO/CreateExamDTO';
import { Lecture } from '../model/lecture';


@Injectable({
  providedIn: 'root'
})
export class ExamsService {
  examsURL = 'http://localhost:8080/exams';
  lecturesURL = 'http://localhost:8080/lectures';
  examForEdit: ExamDTO;

  headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('jwt'))
  constructor(private http: HttpClient) { }

  getExams(): Observable<HttpResponse<HttpResponse<ExamDTO>>> {
    return this.http.get<HttpResponse<ExamDTO>>(this.examsURL,  { observe: 'response', headers: this.headers });
  }

  insert(examDTO: CreateExamDTO): Observable<HttpResponse<CreateExamDTO>> {
    return this.http.post<CreateExamDTO>(this.examsURL, examDTO, { observe: 'response', headers: this.headers });
  }

  deleteExam(id: number): Observable<HttpResponse<String>> {
    return this.http.delete<String>(`${this.examsURL}/${id}`, { observe: 'response', headers: this.headers});
  }

  editExam(params){
    return this.http.put(this.examsURL, params, { observe: 'response', headers: this.headers });
  }


  getLectures(): Observable<HttpResponse<Array<Lecture>>> {
    return this.http.get<Array<Lecture>>(this.lecturesURL,  { observe: 'response', headers: this.headers });
  }
}