import { Component, OnInit } from '@angular/core';
import { ExamDTO } from '../../DTO/ExamDTO';
import { ExamsService } from '../../exams.service';
import { FormControl } from '@angular/forms';
import {Location} from '@angular/common';
import { StudentService } from 'src/app/student/student.service';
import { CourseService } from 'src/app/sharedComponents/courses/course.service';
import { EnrollmentsService } from 'src/app/sharedComponents/enrollments/enrollments.service';

@Component({
  selector: 'app-edit-exam',
  templateUrl: './edit-exam.component.html',
  styleUrls: ['./edit-exam.component.css']
})
export class EditExamComponent implements OnInit {
  student = new FormControl('');
  course = new FormControl('');
  examPoints = new FormControl('');
  labPoints = new FormControl('');
  grade = new FormControl('');

  constructor(private examService: ExamsService, private location: Location) {}

  ngOnInit(): void {
    this.student = new FormControl(this.examService.examForEdit.studentDTO.cardNumber);
    this.course = new FormControl(this.examService.examForEdit.courseDTO.name);
    this.examPoints = new FormControl(this.examService.examForEdit.pointsExam);
    this.labPoints = new FormControl(this.examService.examForEdit.pointsLab);
    this.grade = new FormControl(this.examService.examForEdit.grade);
  }

  submit() {
    let dto = new ExamDTO;
    dto.id = this.examService.examForEdit.id;
    dto.studentDTO = this.student.value;
    dto.courseDTO = this.course.value;
    dto.pointsExam = this.examPoints.value;
    dto.pointsLab = this.labPoints.value;
    dto.grade = this.grade.value;
    this.examService.editExam(dto).subscribe(
      data => {
        this.location.back();
      }, error => {
        alert('error');
        console.log(error)
      }
    )
  }

}