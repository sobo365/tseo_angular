import { StudentDTO } from "../../../student/DTO/StudentDTO";
import { CourseDTO } from "../../courses/DTO/CourseDTO";

export class ExamDTO {
    public id: Number;
    public pointsExam: Number;
    public pointsLab: Number;
    public grade: Number;
    public courseDTO: CourseDTO;
    public studentDTO: StudentDTO;
}