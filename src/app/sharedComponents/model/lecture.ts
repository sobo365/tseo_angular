export class Lecture {
   public id: number;
   public teacher: String;
   public course: String;
}