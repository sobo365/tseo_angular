export class CreateLectureDTO {
    public id: number;
    public teacherId: String;
    public courseId: String;
    public roleId: Number;
 }