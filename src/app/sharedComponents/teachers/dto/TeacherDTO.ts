export class TeacherDTO {
    public id: String;
    public firstName: String;
    public lastName: String;
}
