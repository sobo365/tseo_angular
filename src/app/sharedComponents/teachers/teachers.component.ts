import { Component, OnInit } from '@angular/core';
import { TeachersService } from './teachers.service';
import { TeacherDTO } from './dto/TeacherDTO';
import { Router } from '@angular/router';
import { FormControl} from '@angular/forms';
import { error } from 'protractor';
import {TeacherLecturesDialogComponent} from '../dialogs/teacherLecturesDialog/teacher-lectures-dialog/teacher-lectures-dialog.component'

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {

  teachers : TeacherDTO[];

  displayData = []

  content: any;

  filter = new FormControl('');

  firstNameAsc = true;

  lastNameAsc = true;

  constructor(
    private teachersService: TeachersService, 
    private router: Router,
    private dialog: TeacherLecturesDialogComponent,
    ) { }

  ngOnInit(): void {
    this.loadData();
  }

  addTeacher() {
    this.router.navigate(['administrator/add-teacher'])
  }

  editTeacher(teacher) {
    this.router.navigate(['administrator/add-teacher'])
    this.teachersService.teacherForEdit = teacher;
  }

  deleteTeacher(id) {
    this.teachersService.delete(id).subscribe(
      data => {
        this.loadData();
      }, error => {
        this.loadData();
      }
    )
  }

  teacherLectures(id) {
    this.teachersService.getTeacherLectures(id).subscribe(
      (data : any) => {
        this.content = data.body;
      }, error => {
        console.log(error)
      }
    )
  }

  search(){
    let filtered= []
    for(let i = 0; i < this.teachers.length; i++) {
      if(this.teachers[i].firstName.startsWith(this.filter.value)) {
        filtered.push(this.teachers[i])
      } else if (this.teachers[i].lastName.startsWith(this.filter.value)) {
        filtered.push(this.teachers[i])
      } 
    }
    this.displayData = filtered
  }

  sortFirstName(){
    this.firstNameAsc = !this.firstNameAsc;
    if(this.firstNameAsc) {
      this.displayData.sort(compareA)
    } else {
      this.displayData.sort(compareB)
    }

    function compareB(a, b) {
      if (a.firstName > b.firstName) return 1;
      if (b.firstName > a.firstName) return -1;
    
      return 0;
    }

    function compareA(a, b) {
      if (a.firstName < b.firstName) return 1;
      if (b.firstName < a.firstName) return -1;
    
      return 0;
    }
  }

  sortLastName() {
    this.lastNameAsc = !this.lastNameAsc;
    if(this.lastNameAsc) {
      this.displayData.sort(compareB)
    } else {
      this.displayData.sort(compareA)
    }

    function compareB(a, b) {
      if (a.lastName > b.lastName) return 1;
      if (b.lastName > a.lastName) return -1;
    
      return 0;
    }

    function compareA(a, b) {
      if (a.lastName < b.lastName) return 1;
      if (b.lastName < a.lastName) return -1;
    
      return 0;
    }
  }

  loadData(): void {
    this.teachersService.getAll().subscribe(
      (data: any) => {
        console.log(data.body)
        this.teachers = data.body;
        this.displayData = data.body;
      }, error => {
        alert('There was an error!')
        console.log(error)
      }
    )
  }

}
