import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { TeacherDTO } from './dto/TeacherDTO';

@Injectable({
  providedIn: 'root'
})
export class TeachersService {

  connectionURL = 'http://localhost:8080/teachers';

  headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + localStorage.getItem('jwt'))

  constructor(private http: HttpClient) { }

  teacherForEdit:TeacherDTO;

  getAll(): Observable<HttpResponse<TeacherDTO>> {
    return this.http.get<TeacherDTO>(this.connectionURL,  { observe: 'response', headers: this.headers })
  }

  getTeacherLectures(id: String) {
    return this.http.get(`${this.connectionURL}/lectures/${id}`,  { observe: 'response', headers: this.headers })
  }

  insert(params){
    return this.http.post(this.connectionURL,params, { observe: 'response', headers: this.headers });
  } 

  update(params){
    return this.http.put(this.connectionURL,params, { observe: 'response', headers: this.headers });
  } 

  delete(id: String) {
    return this.http.delete(`${this.connectionURL}/${id}`, { observe: 'response', headers: this.headers});
  }
}
