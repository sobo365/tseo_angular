import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { FormControl} from '@angular/forms';
import { TeachersService } from '../teachers.service';
import { TeacherDTO } from '../dto/TeacherDTO';
import { distinct } from 'rxjs/operators';

@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html',
  styleUrls: ['./add-teacher.component.css']
})
export class AddTeacherComponent implements OnInit {

  firstname = new FormControl('');
  lastname = new FormControl('');

  constructor(private teachersService: TeachersService, private location: Location) { }

  ngOnInit(): void {
    if(this.teachersService.teacherForEdit != null ) {
      this.firstname = new FormControl(this.teachersService.teacherForEdit.firstName);
      this.lastname = new FormControl(this.teachersService.teacherForEdit.lastName);
    }
  }

  submit() {
    if(this.teachersService.teacherForEdit == null){
      let dto = new TeacherDTO;
      dto.firstName = this.firstname.value;
      dto.lastName = this.lastname.value;
      this.teachersService.insert(dto).subscribe(
        data => {
          this.location.back();
        }, error => {
          alert('error')
          console.log(error)
        }
      ) 
    } else {
      let dto = new TeacherDTO;
      dto.id = this.teachersService.teacherForEdit.id;
      dto.lastName = this.lastname.value;
      dto.firstName = this.firstname.value;
      this.teachersService.update(dto).subscribe(
        data => {
          this.location.back();
        }, error => {
          alert('error');
          console.log(error)
        }
      )
    }
  }

  ngOnDestroy() {
    this.teachersService.teacherForEdit = null;
  }

  backBtn(){
    this.location.back();
  }

}
