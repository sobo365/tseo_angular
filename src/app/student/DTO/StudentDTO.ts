export class StudentDTO{
    public id: Number;
    public firstName: String;
    public lastName: String;
    public cardNumber: String;
}