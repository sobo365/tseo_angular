import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StudentDTO } from './DTO/StudentDTO';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  studentsURL = 'http://localhost:8080/students';

  headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('jwt'))
  constructor(private http: HttpClient) { }

  getStudents(): Observable<HttpResponse<Array<StudentDTO>>> {
    return this.http.get<Array<StudentDTO>>(this.studentsURL,  { observe: 'response', headers: this.headers });
  }
}

