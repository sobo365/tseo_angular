import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { CreateExamComponent } from '../sharedComponents/exams/createExamComponent/create-exam/create-exam.component';
import { ExamsStudentComponent } from '../sharedComponents/exams/student/exams-student/exams-student.component';
import { EnrollmentsStudentComponent } from '../sharedComponents/enrollments/student/enrollments-student/enrollments-student.component';
import { ProfileComponent } from './profile/profile/profile.component';
import { CreateEnrollmentComponent } from '../sharedComponents/enrollments/createEnrollment/create-enrollment/create-enrollment.component';
import { ExamsStudentRegisterComponent } from '../sharedComponents/exams/student/exams-student/registered/exams-student-register/exams-student-register.component';
import { DocumentsComponent } from './documents/documents.component';

const routes: Routes = [
  {path: '', component: MainComponent, children: [
  { path: 'profile', component: ProfileComponent },
  { path: 'enrollments', component: EnrollmentsStudentComponent },
  { path: 'createEnrollment', component: CreateEnrollmentComponent},
  { path: 'exams', component: ExamsStudentComponent },
  { path: 'registerExam', component: CreateExamComponent },
  { path: 'examsStudentRegister', component: ExamsStudentRegisterComponent },
  { path: 'documents', component: DocumentsComponent },
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
