import { Component, OnInit } from '@angular/core';
import { StudentDTO } from '../../DTO/StudentDTO';
import { StudentService } from '../../student.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  students: StudentDTO[];

  readLocalStorageValue(key) {
    return localStorage.getItem('pid');
  }

  constructor(private studentsService: StudentService) { }

  getStudents(): void {
    this.studentsService.getStudents().subscribe(
      (data: any) => {
        this.students = data.body;
      }
    )}

  ngOnInit(): void {
    this.getStudents();
  }
  
}