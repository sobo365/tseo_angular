import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DocumentsService } from '../../sharedComponents/dialogs/studentDocumentsDialog/documents.service'
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {

  constructor(private service: DocumentsService, private sanitizer: DomSanitizer) { }

  documents = [];

  doc = new FormControl('');

  documentData;

  documentName;

  ngOnInit(): void {
    this.loadDocuments();
  }

  toggleIframe = (position) => {
    this.documents[position].display = !this.documents[position].display;
  }

  handleFileInput = (files) => {

    this.documentName = files.item(0).name;

    this.f2b(files.item(0)).then((result: String) => {
      this.documentData = result.split(',')[1]

    })
  }

  upload = () => {
    let document = {
      studentId: localStorage.getItem('pid'),
      name: this.documentName,
      data: this.documentData,
    }
    console.log(document)
    this.service.insert(document).subscribe(
      data => {
        alert('ok')
      }, error => {
        alert('error')
        console.log(error)
      }
    )
  }

  f2b = (file) => {
    return new Promise(resolve => {
      let reader = new FileReader();

      reader.onload = (event) => {
        resolve(event.target.result);
      }

      reader.readAsDataURL(file)
    })
  }

  loadDocuments = () => {
    this.documents = [];
    this.service.getDocuments(localStorage.getItem('pid')).subscribe(
      (data: any) => {
        console.log(data.body)
        for (let i = 0; i < data.body.length; i++) {
          let d = {
            position: i,
            name: data.body[i].name,
            data: this.sanitizer.bypassSecurityTrustResourceUrl(`data:application/pdf;base64, ${data.body[i].path}`),
            display: false,
          }
          this.documents.push(d);
        }
      }, error => {
        console.log(error)
      }
    )
  }

}
